import React, { useState } from "react";
import { useQuery } from "react-query";
import Planet from "./Planet";

const fetchPlanets = async (page) => {
  console.log(page);
  const res = await fetch(`http://swapi.dev/api/planets/?page=${page}`);
  return res.json();
};

function Planets() {
  const [page, setPage] = useState(1);
  const { data, status, isPreviousData, isFetching } = useQuery(
    ["planets", page],
    () => fetchPlanets(page)
  );
  //  {
  //     staleTime: 0,
  //     cacheTime: 10,
  //     onSuccess: () => console.log('data fetched')
  // }

  return (
    <div>
      <h2>Planets</h2>
      {/* <p>{ status }</p> */}
      <button disabled={page === 1} onClick={() => setPage(page - 1)}>
        Previous
      </button>
      <span> Curren page: {page}</span>
      <button
        disabled={page === data?.count / 10}
        onClick={() => setPage(page + 1)}
      >
        Next
      </button>
      <button onClick={() => setPage(3)}>page 3</button>

      {status === "loading" && <div> Loading data... </div>}
      {status === "error" && <div> Error fetching data </div>}

      {status === "success" && (
        <div>
          {data.results.map((planet) => (
            <Planet key={planet.name} planet={planet} />
          ))}
        </div>
      )}
    </div>
  );
}

export default Planets;
