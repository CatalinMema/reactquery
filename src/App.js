import React, { useState } from "react";
import { QueryClient, QueryClientProvider } from "react-query";
import Example1 from "./components/Example1";
import Navbar from "./components/Navbar";
import People from "./components/People";
import Planets from "./components/Planets";
import { ReactQueryDevtools } from 'react-query-devtools'

const queryClient = new QueryClient();
function App() {
  const [page, setPage] = useState('planets')

  return (
      <QueryClientProvider client={queryClient}>
        {/* <Example1 /> */}
      
        <div className="App">
          <h1>Star Wars Info</h1>
          <Navbar setPage={setPage} />
          <div className="content">
          {page === 'planets' ? <Planets /> : <People />}
          </div>
        </div>
      </QueryClientProvider>
  );
}

export default App;
